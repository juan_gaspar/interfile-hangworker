﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace Interfile.HangWorker
{
    public partial class Service1 : ServiceBase
    {
        int interval = int.Parse(ConfigurationManager.AppSettings["miliSecInterval"]);
        bool logs = ConfigurationManager.AppSettings["logs"] != null
                        ? ConfigurationManager.AppSettings["logs"].ToLower().Equals("true")
                        : false;
        public Service1()
        {
            InitializeComponent();

            if (logs)
            {
                EventLog appLog = new EventLog("Application");
                appLog.Source = GetEventSource();
                appLog.WriteEntry("Service Started with invertal " + interval + " milliseconds...");
            }
        }

        protected override void OnStart(string[] args)
        {
            Call();
        }

        protected override void OnStop()
        {
            if (logs)
            {
                EventLog appLog = new EventLog("Application");
                appLog.Source = GetEventSource();
                appLog.WriteEntry("Service Stoped...");
            }
        }

        private void Do()
        {
            Thread.Sleep(interval);
            Call();
        }

        private void Call()
        {
            Task.Factory.StartNew(() =>
            {
                if (logs)
                {
                    EventLog appLog = new EventLog("Application");
                    appLog.Source = GetEventSource();
                    appLog.WriteEntry("Starting call...");
                }

                var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["hangUrl"]);

                var response = (HttpWebResponse)request.GetResponse();


                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (logs)
                {
                    EventLog appLog = new EventLog("Application");
                    appLog.Source = GetEventSource();
                    appLog.WriteEntry("Call finished... Status: " + response.StatusCode.ToString());
                }
            }).ContinueWith(task => Do());
        }


        private string GetEventSource()
        {
            string eventSource = "HangWorker";
            bool sourceExists;
            try
            {
                // searching the source throws a security exception ONLY if not exists!
                sourceExists = EventLog.SourceExists(eventSource);
                if (!sourceExists)
                {   // no exception until yet means the user as admin privilege
                    EventLog.CreateEventSource(eventSource, "Application");
                }
            }
            catch (SecurityException)
            {
                eventSource = "Application";
            }

            return eventSource;
        }
    }
}
